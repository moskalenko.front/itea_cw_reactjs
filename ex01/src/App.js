import React, { Component } from 'react';
import Guest from './components/Guest'
import './App.scss';
import guests from './guests.json';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      search: '',
      guests: guests
    }
  }

  onSearch = (event) => {
    const searchQuery = event.target.value.toLowerCase(),
          searchedGuests = guests.filter(guest => guest.name.toLowerCase().indexOf(searchQuery) !== -1);

    this.setState({
      guests: searchedGuests
    })
  }
  render() {
    const {guests} = this.state;
    
    return (
      <div>
        <h1>Список гостей</h1>
        <label>
          Поиск:
          <input type="text" onChange={this.onSearch} />
        </label>
        <div className="guests">
          {
            guests.length ?
              guests.map(guest => <Guest guest={guest} key={guest.index}/>) :
              'Ничего не найдено'
          }
        </div>
      </div>
    );
  }
}

export default App;
