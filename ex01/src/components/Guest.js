import React, {Component} from 'react'

export default class Guest extends Component {
    constructor(props) {
        super(props);
        this.state = {
            done: false
        }
    }
    render() {
        const {guest} = this.props;
        const {done} = this.state;

        return (
            <div className={done ? 'guest guest--done' : 'guest'}>
                <p>
                    {guest.name};
                </p>
                <button onClick={() => this.setState({done: true})} disabled={done}>
                    Прибыл
                </button>
            </div>
        );
    }
}