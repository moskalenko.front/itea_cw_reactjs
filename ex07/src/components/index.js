import React from 'react';
import { Switch, Route, Link } from 'react-router-dom';
import Form from './pages/form';
import Preview from './pages/preview';
import MainPage from './main';
import NotFound from './common/notFound';
import Localization from './pages/localization';

const Root = ({ match, location }) => {
  return(
    <>
      <nav>
        <Link to={`${match.url}/form`}>form</Link>
        {' | '}
        <Link to={`${match.url}/preview`}>preview</Link>
        {' | '}
        <Link to={`${match.url}/lang`}>lang</Link>
      </nav>
      <Switch>
        <Route path={`${match.url}/`} exact component={MainPage}/>
        <Route path={`${match.url}/form`} exact component={Form}/>
        <Route path={`${match.url}/preview`} exact component={Preview}/>
        <Route path={`${match.url}/lang`} exact component={Localization}/>
        <Route component={NotFound}/>
      </Switch>
    </>
  )
}

export default Root;
