import React from 'react';
import { Field, reduxForm, FormSection } from 'redux-form';
import {connect} from 'react-redux';
import {FORM_SAVE} from './../../../constants'

const UserForm  = ({handleSubmit}) => (
    <form onSubmit={handleSubmit}>
        <div>
            <label htmlFor="firstName">firstName</label>
            <Field name="firstName" component="input" type="text" />
        </div>
        <div>
            <label htmlFor="lastName">lastName</label>
            <Field name="lastName" component="input" type="text" />
        </div>
        <FormSection name="education">
            <div>
                <label htmlFor="university">university</label>
                <Field name="university" component="input" type="text" />
            </div>
            <div>
                <label htmlFor="country">country</label>
                <Field name="country" component="input" type="text" />
            </div>
            <div>
                <label htmlFor="city">city</label>
                <Field name="city" component="input" type="text" />
            </div>
        </FormSection>
        <FormSection name="address">
            <div>
                <label htmlFor="country">country</label>
                <Field name="country" component="input" type="text" />
            </div>
            <div>
                <label htmlFor="city">city</label>
                <Field name="city" component="input" type="text" />
            </div>
            <div>
                <label htmlFor="street">street</label>
                <Field name="street" component="input" type="text" />
            </div>
        </FormSection>
        <button type="submit">Submit</button>
    </form>
);

const UserFormRedux = reduxForm({form: 'userData'})(UserForm);

const Form = ({dispatch}) => {
    const submit = values => {
        dispatch({
            type: FORM_SAVE,
            data: values
        })

    }
    return (
        <UserFormRedux onSubmit={submit}/>
    )
};

export default connect()(Form);