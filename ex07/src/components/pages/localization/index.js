import React from 'react';

import { FormattedMessage } from 'react-intl';


const Localization = () => {
  return(
    <div>
      <h1><FormattedMessage id="localization"/></h1>
      <FormattedMessage id="localization" defaultMessage="Navigation">
        { msg => (
          <div>{msg}</div>
        )}
      </FormattedMessage>
    </div>
  );
}

export default Localization;