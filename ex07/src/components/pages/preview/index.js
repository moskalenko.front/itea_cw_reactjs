import React from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';

const Preview = ({formData, match}) => {
    console.log(match.url.split('/')[1]);
    return(
    <div>
        {
        formData ?
        JSON.stringify(formData) : (
            <>
                Нет данных формы <br />
                Введите данные:
                <Link to={`/${match.url.split('/')[1]}/form`}>форма</Link>
                
            </>
        )
        }
    </div>)
};
const mapStateToProps = (state, ownProps) => {
    return {
        formData: state.userData
    };
};


export default connect(mapStateToProps)(Preview);