import React from 'react';
import Root from './index.js';
import { Route } from 'react-router-dom';
/*
  Localization Support
*/
import { addLocaleData, IntlProvider } from 'react-intl'
import en from 'react-intl/locale-data/en';
import uk from 'react-intl/locale-data/uk';
import ru from 'react-intl/locale-data/ru';
import translations from '../translations';

// Init locales
addLocaleData([...en, ...uk, ...ru]);

const IntlComponent = ({match, ...rest}) => {
  // let locale = match.params.locale === 'ua' ? 'uk' : match.params.locale;
  let locale = match.params.locale;
  return(
    <IntlProvider locale={locale} messages={translations[locale]}>
      <Route component={Root} />
    </IntlProvider>
  );
}


export default IntlComponent;
