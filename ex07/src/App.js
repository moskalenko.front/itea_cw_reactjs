import React, { Component } from 'react';
import LocalizationComponent from './components/localizationComponent';


import './styles/index.css';
import './styles/app.css';

import { Provider } from "react-redux";
import store from './redux/store';
import { BrowserRouter } from 'react-router-dom';

const supportsHistory = 'pushState' in window.history;

class App extends Component {
  render() {
    return (
      <div>
        <Provider store={store}>
          <BrowserRouter forceRefrech={!supportsHistory}>
            <LocalizationComponent/>
          </BrowserRouter>
        </Provider>
      </div>
    );
  }
}

export default App;
