import Common from './common';
import config from '../config';

const TranslationsArray = [Common];
const ConnectedTranslations = {uk:{}, ru:{}, en:{}};

TranslationsArray.forEach( translationObject => {
  config.supprotedLangs.forEach( lang => {
    ConnectedTranslations[lang] = {
      ...ConnectedTranslations[lang],
      ...translationObject[lang]
    };
  })
});

console.log('start:', TranslationsArray, 'finish:', ConnectedTranslations );

export default ConnectedTranslations;
