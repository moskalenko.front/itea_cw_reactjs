export default {
    en: {
      'common.menu': 'Navigation',
      'localization': 'Localization',
    },
    uk: {
      'common.menu': 'Навігація',
      'localization': 'Локалізація',
    },
    ru: {
      'common.menu': 'Навигация',
      'localization': 'Локализация',
    }
  }
  