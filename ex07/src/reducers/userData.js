import {FORM_SAVE} from './../constants'

const initState = null;

const userData = (state = initState, action) => {
    switch(action.type) {
        case FORM_SAVE: 
            return {
                ...state,
                ...action.data
            }
        default: 
            return state;
    }
}

export default userData;