import React, { Component } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import Routes from './routes';

import store from './Redux/store';
import { Provider } from "react-redux";

import Navigation from './Components/Navigation';
import AddNote from './Components/AddNote';

const supportsHistory = 'pushState' in window.history;

class App extends Component {
  render() {
    return (
       <Provider store={store}>
         <BrowserRouter forceRefrech={!supportsHistory}>
         <div>
            <Navigation />
            <div className="container">
               <AddNote />
               <Switch>
                  {
                     Routes.map( (route, index ) => {
                        return <Route key={index} {...route} />;
                     })
                  }
               </Switch>
            </div>
         </div>
         </BrowserRouter>
      </Provider>
    );
  }
}

export default App;
