import {
   ADD_NOTE, REMOVE_NOTE,
   DONE_NOTE
 
 } from '../constants';
 
 const initialState = {
   notes: [] 
 };
 
 function reducer(state = initialState, action){
   switch( action.type ){
 
      case ADD_NOTE:
         const newNote = {
            text: action.data,
            isDone: false
         }

         return {
            ...state,
            notes: [...state.notes, newNote]
         }

      case REMOVE_NOTE:
         state.notes.splice(action.data, 1);
         return {
            ...state,
            notes: [...state.notes]
         }
  
      case DONE_NOTE:
         state.notes[action.data].isDone = true;
         return {
            ...state,
            notes: [...state.notes]
         }
 
      default:
         return state;
   }
 };
 
 
 
 export default reducer;
 