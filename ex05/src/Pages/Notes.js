import React from 'react';
import { connect } from 'react-redux';
import Note from '../Components/Note'


function Notes({notes}) {
   return(
      <div>
         <h1>Notes</h1>
         <ul className="notes">
            {
               notes.map((note, index) => (
                  <Note key={index} index={index} text={note.text} isDone={note.isDone} />
               ))
            }
         </ul>
      </div>
   )
}


// Redux
const mapStateToProps = (state, ownProps) => ({
   notes: state.notes
});
  
const ConnectedNotes = connect(mapStateToProps)(Notes);
export default ConnectedNotes;