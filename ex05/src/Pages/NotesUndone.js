import React from 'react';
import { connect } from 'react-redux';
import Note from '../Components/Note';

const NotesUndone = ({notes}) => {
   const notesUndone = notes.map((note, index) => {
      if(!note.isDone) {
         return <Note key={index} index={index} text={note.text} isDone={note.isDone} />
      }
   })
   return(
   <div>
      <h1>Notes Undone</h1>
      { 
         notesUndone.length ? (
            <ul className="notes">
               {notesUndone}
            </ul>
         ) : 'Not data'
      }
   </div>
   )
};

// Redux
const mapStateToProps = (state, ownProps) => ({
   notes: state.notes
});
  
const ConnectedNotesUndone = connect(mapStateToProps)(NotesUndone);

export default ConnectedNotesUndone;