import Notes from './Pages/Notes';
import NotesDone from './Pages/NotesDone'
import NotesUndone from './Pages/NotesUndone'

const Routes = [
   {
     path: '/',
     exact: true,
     component: Notes
   },
   {
      path: '/notes-done',
      exact: true,
      component: NotesDone
   },
   {
      path: '/notes-undone',
      exact: true,
      component: NotesUndone
   }
 ];
 
 export default Routes;