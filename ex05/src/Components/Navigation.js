import React from 'react'
import {NavLink} from 'react-router-dom'

const Navigation = () => (
   <nav>
      <NavLink exact activeClassName="active" to='/'>All</NavLink>
      <NavLink activeClassName="active" to='/notes-done'>Notes Done</NavLink>
      <NavLink activeClassName="active" to='/notes-undone'>Notes Undone</NavLink>
   </nav>
);

export default Navigation;