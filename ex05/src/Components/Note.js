import React from 'react'
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { DONE_NOTE, REMOVE_NOTE } from '../Redux/constants';

const Note = ({text, index, isDone, onDone, onRemove}) => {
   const onDoneNote = () => {
      onDone(index);
   }
   const onRemoveNote = () => {
      onRemove(index);
   }
   return (
      <li className={isDone ? 'done' : null}>
         <span>
            { text }
         </span>
         <button 
            className="btn--done"
            disabled={isDone} 
            onClick={onDoneNote}
         >
            Done
         </button>
         <button className="btn--remove" onClick={onRemoveNote}>Delete</button>
      </li>
   );
}

Note.propTypes = {
   text: PropTypes.string.isRequired,
   index: PropTypes.number.isRequired,
   isDone: PropTypes.bool.isRequired,
   onDone: PropTypes.func.isRequired, 
   onRemove: PropTypes.func.isRequired
};

// Redux
const mapDispatchToProps = (dispatch, ownProps) => ({
   onDone: (index) => {
      dispatch({
         type: DONE_NOTE,
         data: index
      })
   },
   onRemove: (index) => {
      dispatch({
         type: REMOVE_NOTE,
         data: index
      })
   } 
});
const ConnectedNote = connect(null, mapDispatchToProps)(Note);

export default ConnectedNote;