import React from 'react'
import { connect } from 'react-redux';
import { ADD_NOTE } from '../Redux/constants';

const AddNote = ({addNote}) => {
   const noteRef = React.createRef();
   const onAddNote = () => {
      const noteName = noteRef.current.value;
      noteRef.current.value = null;
      addNote(noteName);
   }
   return (
      <label>
         <input ref={noteRef} type="text" name="noteName" placeholder="add new note" />
         <button onClick={onAddNote}>+</button>
      </label>
   )
}


// Redux
const mapDispatchToProps = (dispatch, ownProps) => ({
 addNote: (name) => {
   dispatch({
     type: ADD_NOTE,
     data: name
   })
 } 
});

const ConnectedAddNote = connect(null, mapDispatchToProps)(AddNote);


export default ConnectedAddNote;