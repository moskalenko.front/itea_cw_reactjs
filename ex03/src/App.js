import React, { Component } from 'react';
import Toggle from './Toggle'
import ToggleItem from './Toggle/components/toggleItem'
import Input from './components/input'

import './App.css';

class App extends Component {
   constructor(props) {
      super(props);
      this.state = {
         formData: {
            login: null,
            password: null,
            gender: 'male',
            age: null,
            layout: 'center',
            langues: null
         },
      }
   }

   changeState = (event) => {
      let newToggleStatus = event.target.dataset.value;
      let toggleName = event.target.dataset.parent;
      this.setState({
         formData: {
            ...this.state.formData,
            [toggleName]: newToggleStatus
         }
      });
   }

   onChangeHandler = (event) => {
      let value = event.target.value;
      let name = event.target.name;

      this.setState({
         [formData]:{
         ...this.state.formData,
         [name]: value
         }
      });
   }

   submit = (e) => {
      e.preventDefault();
      console.log(this.state.formData);
   }

   render() {
      const {formData} = this.state;
      return ( 
         <div>
            <form onSubmit={this.submit}>
               <Input
                  type="text"
                  name="login"
                  onChangeHandler = {this.onChangeHandler}
               />
               <Input
                  type="password"
                  name="password"
                  onChangeHandler = {this.onChangeHandler}
               />
               <Toggle 
                  name = "gender"
                  action = {this.changeState}
                  activeState = {formData.gender}
               >
                  <ToggleItem
                     name="male"
                  />
                  <ToggleItem
                     name="female"
                  />
               </Toggle>
               <Input
                  type="number"
                  name="age"
                  onChangeHandler = {this.onChangeHandler}
               />
               <Toggle 
                  name = "layout"
                  action = {this.changeState}
                  activeState = {formData.layout}
               >
                  <ToggleItem
                     name="left"
                  />
                  <ToggleItem
                     name="center"
                  />
                  <ToggleItem
                     name="right"
                  />
                  <ToggleItem
                     name="baseline"
                  />
               </Toggle>
               <Input
                  type="text"
                  name="langues"
                  onChangeHandler = {this.onChangeHandler}
               />
               <div>
                  <button type="submit">Ok</button>
               </div>
            </form>
         </div>
      );
   }
}

export default App;
