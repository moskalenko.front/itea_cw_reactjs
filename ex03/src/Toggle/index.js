import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './style.css';

class Toggle extends Component {
  
    render() {
        const {name, activeState, children, action} = this.props;
        return (
            <div className="toggle__wrap">
                <h3>
                    {name}
                </h3>
                <div className="toggle">
                    {
                        React.Children.map(
                            children,
                            ( ChildrenItem => {
                                if(ChildrenItem.props.name === activeState){
                                    return React.cloneElement(ChildrenItem, {
                                        name: ChildrenItem.props.name,
                                        active: true,
                                        onChangeStatus: action,
                                        parentName: name
                                    })
                                } else {
                                  return React.cloneElement(ChildrenItem, {
                                    name: ChildrenItem.props.name,
                                    onChangeStatus: action,
                                    parentName: name
                                  })
                                }
                            })
                        )
                    }
                </div>
            </div>
        );
    }
  }
  
  export default Toggle;

  Toggle.propTypes = {
      // title in label
    name: PropTypes.string.isRequired,
    action: PropTypes.func.isRequired,
    activeState: PropTypes.string,
    children: PropTypes.arrayOf(PropTypes.element)
  };