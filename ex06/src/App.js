import React, { Component } from 'react';
import Root from './components'

//Styles
import './styles/index.css';

//Redux 
import { Provider } from "react-redux";
import store from './redux/store';

//Router
import { BrowserRouter } from 'react-router-dom';

const supportsHistory = 'pushState' in window.history;


class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <BrowserRouter forceRefrech={!supportsHistory}> 
          <Root/>
        </BrowserRouter>
      </Provider>
    );
  }
}

export default App;
