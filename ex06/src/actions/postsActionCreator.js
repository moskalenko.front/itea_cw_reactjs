import { 
  PROMISE, 
  POSTS_REQ, POSTS_SUCC, POSTS_ERR,
  USER_POSTS_REQ, USER_POSTS_SUCC, USER_POSTS_ERR
} from '../constants';

  const BASE_URL = 'https://jsonplaceholder.typicode.com';

  const postsActionCreator = {
    fetchUserPosts: (id) => (dispatch, getState) => {
      dispatch({
        type: PROMISE,
        actions: [USER_POSTS_REQ, USER_POSTS_SUCC, USER_POSTS_ERR],
        promise: fetch(`${BASE_URL}/posts?userId=${id}`)
          .then( res => res.json() )
      })
    },
    fetchPromise: () => (dispatch, getState) => {
      dispatch({
        type: PROMISE,
        actions: [POSTS_REQ, POSTS_SUCC, POSTS_ERR],
        promise: fetch(`${BASE_URL}/posts` )
          .then( res => res.json() )
      })
    }
  }
  
  export default postsActionCreator;
  