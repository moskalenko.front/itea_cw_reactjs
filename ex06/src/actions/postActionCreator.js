import { PROMISE, POST_REQ, POST_SUCC, POST_ERR } from '../constants';

  const BASE_URL = 'https://jsonplaceholder.typicode.com';

  const postsActionCreator = {
    fetchPromise: (id) => (dispatch, getState) => {
      dispatch({
        type: PROMISE,
        actions: [POST_REQ, POST_SUCC, POST_ERR],
        promise: fetch(`${BASE_URL}/posts/${id}` )
          .then( res => res.json() )
      })
    }
  }
  
  export default postsActionCreator;
  