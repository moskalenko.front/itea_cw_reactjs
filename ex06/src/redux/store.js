import { createStore, applyMiddleware, compose } from "redux";
import reducer from '../reducers';

//Middleware
import thunk from 'redux-thunk';
import promiseMiddleware from './middleware/promise'

//redux-dev-tools
const composeEnhancers = process.env.NODE_ENV !== 'production' && typeof window !== 'undefined' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__  ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ : compose;

//applyMiddleware 
const middleware = applyMiddleware(thunk, promiseMiddleware);

const store = createStore( reducer, composeEnhancers(middleware) );

export default store;
