import React from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import postsActionCreator from '../../actions/postsActionCreator';

class User extends React.Component {
    componentDidMount() {
        const userId = this.props.match.params.id;
        this.props.fetchUserPosts(userId);
    }
    render() {
        const userPosts = this.props.userPosts.data;
        return(
            <div>
                <h1>User Posts</h1>
                <ul>
                    {
                        userPosts.map((post, indx) => (
                            <li key={indx}>
                                <Link to={`/posts/${post.id}`}>
                                    { post.title }
                                </Link>
                            </li>
                        ))
                    }
                </ul>
            </div>
        )
    }
}
  
const mapStateToProps = (state, ownProps) => {
    return {
        userPosts: state.userPosts
    };
};
const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        fetchUserPosts: (id) => {
            dispatch( postsActionCreator.fetchUserPosts(id));
        }
    };
};
  
const ConnectedUser = connect(  mapStateToProps,  mapDispatchToProps)(User);
export default ConnectedUser;
  