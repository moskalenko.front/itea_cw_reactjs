import React from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import postsActionCreator from '../../actions/postsActionCreator';

class Posts extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            countShowPosts: 50,
            isShowBtnMore: true
        }
    }
    onShowMore = () => {
        const step = 25, 
            countPosts = this.state.countShowPosts + step,
            posts = this.props.posts.data || [];

        if( countPosts < posts.length ) {
            this.setState({
                countShowPosts: countPosts
            })
        } else {
            this.setState({
                countShowPosts: posts.length,
                isShowBtnMore: false
            })
        }
    }
    componentDidMount() {
        const { fetchPromise } = this.props;
        fetchPromise();
    }
    render() {
        const {onShowMore} = this;
        const {countShowPosts, isShowBtnMore} = this.state;
        const { posts } = this.props;

        return(
            <div>
                <ul>
                    {
                        posts.data.map((post, indx) => (
                            ((indx + 1) >=  countShowPosts) ? null :
                            <li key={indx}>
                                <Link to={`/posts/${post.id}`}>
                                    { post.title }
                                </Link>
                            </li>
                        ))
                    }
                </ul>
                {
                    (isShowBtnMore) && 
                    (<button onClick={onShowMore}>
                        show more
                    </button>)
                }
            </div>
        )
    }
}
  
const mapStateToProps = (state, ownProps) => {
    return {
        posts: state.posts
    };
};
const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        fetchPromise: () => {
            dispatch( postsActionCreator.fetchPromise());
        }
    };
};
  
const ConnectedPosts = connect(  mapStateToProps,  mapDispatchToProps)(Posts);
export default ConnectedPosts;
  