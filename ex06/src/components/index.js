import React from 'react';
import Main from './main';
import Posts from './posts';
import Post from './post';
import User from './user';

import { Switch, Route, Link } from 'react-router-dom';

const Root = () => {
    return(
        <>
            <nav>
                <Link to="/">Main</Link>
                <Link to="/posts">Posts</Link>
            </nav>
            <Switch>
                <Route path="/" exact component={Main}/>
                <Route path="/posts" exact component={Posts}/>
                <Route path="/posts/:id" exact component={Post}/>
                <Route path="/users/:id" exact component={User}/>
            </Switch>
        </>
    )    
}

export default Root;