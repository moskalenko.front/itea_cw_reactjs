import React from 'react';
import { connect } from 'react-redux';
import {Link} from 'react-router-dom';
import postActionCreator from '../../actions/postActionCreator';

class Post extends React.Component{
    componentDidMount() {
        const postId = this.props.match.params.id;
        this.props.fetchPromise(postId);
    }
    render() {
        const {post} = this.props;

        return(
            <table>
                <tbody>
                    <tr>
                        <td>ID</td>
                        <td>{post.data.id}</td>
                    </tr>
                    <tr>
                        <td>userID</td>
                        <td>
                            <Link to={`/users/${post.data.userId}`}>
                                {post.data.userId}
                            </Link>
                        </td>
                    </tr>
                    <tr>
                        <td>Title</td>
                        <td>{post.data.title}</td>
                    </tr>
                    <tr>
                        <td>Body</td>
                        <td>{post.data.body}</td>
                    </tr>
                </tbody>
            </table>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        post: state.post
    };
};
const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        fetchPromise: (id) => {
            dispatch( postActionCreator.fetchPromise(id));
        }
    };
};
  
const ConnectedPost = connect(  mapStateToProps,  mapDispatchToProps)(Post);
export default ConnectedPost;
 