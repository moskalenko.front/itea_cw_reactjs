export const USER_POSTS_REQ = 'USER_POSTS_REQ';
export const USER_POSTS_SUCC = 'USER_POSTS_SUCC';
export const USER_POSTS_ERR = 'USER_POSTS_ERR';

export const POSTS_REQ = 'POSTS_REQ';
export const POSTS_SUCC = 'POSTS_SUCC';
export const POSTS_ERR = 'POSTS_ERR';

export const POST_REQ = 'POST_REQ';
export const POST_SUCC = 'POST_SUCC';
export const POST_ERR = 'POST_ERR';

export const PROMISE = 'PROMISE';