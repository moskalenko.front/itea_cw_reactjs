import {POST_REQ, POST_SUCC, POST_ERR} from '../../constants'

const initState = {
    loaded: false,
    loading: false,
    data: {},
    errors: []
}

const post = (state = initState, action) => {
    switch(action.type) {
        case POST_REQ:
            return {
                ...state,
                loading: true
            }

        case POST_SUCC:
            return {
                ...state,
                data: action.data,
                loading: false,
                loaded: true
            }
    

        case POST_ERR:
            return {
                ...state,
                errors: action.error
            }
    
        default:
            return state;

    }
}

export default post;