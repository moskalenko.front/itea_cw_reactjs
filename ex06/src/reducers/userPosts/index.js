import {USER_POSTS_REQ, USER_POSTS_SUCC, USER_POSTS_ERR} from '../../constants'

const initState = {
    loaded: false,
    loading: false,
    data: [],
    errors: []
}

const userPosts = (state = initState, action) => {
    switch(action.type) {
        case USER_POSTS_REQ:
            return {
                ...state,
                loading: true
            }
        case USER_POSTS_SUCC:
            return {
                ...state,
                data: action.data,
                loading: false,
                loaded: true
            }
        case USER_POSTS_ERR:
            return {
                ...state,
                errors: action.error
            }    
        default:
            return state;

    }
}

export default userPosts;