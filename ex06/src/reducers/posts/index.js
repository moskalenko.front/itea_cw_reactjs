import {POSTS_REQ, POSTS_SUCC, POSTS_ERR} from '../../constants'

const initState = {
    loaded: false,
    loading: false,
    data: [],
    errors: []
}

const posts = (state = initState, action) => {
    switch(action.type) {
        case POSTS_REQ:
            return {
                ...state,
                loading: true
            }
        case POSTS_SUCC:
            return {
                ...state,
                data: action.data,
                loading: false,
                loaded: true
            }
        case POSTS_ERR:
            return {
                ...state,
                errors: action.error
            }    
        default:
            return state;

    }
}

export default posts;