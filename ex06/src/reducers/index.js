import { combineReducers } from 'redux';
import posts from './posts';
import post from './post';
import userPosts from './userPosts';

const reducer = combineReducers({
  posts,
  post,
  userPosts
});

export default reducer;
