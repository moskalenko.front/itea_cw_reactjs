import React from 'react';
import PropTypes from 'prop-types';
import './style.css'

const RatingStars = ({action, rank, id}) => {
   const MAX_STARS = 5;
   let stars = {};
   
   for(let i = 1; i <= MAX_STARS; i++) {
      stars[i] =
         <span 
            key={(MAX_STARS - i) + 1} 
            className={ rank === (MAX_STARS - i) + 1 ? 'curr' : null}
            data-rank={(MAX_STARS - i) + 1}
            onClick={(event) => {
               changeRating(event)
               action(event);
            }}
         >☆</span>
      ;
   }
   
   function changeRating(event) {
      Array.from(event.target.parentNode.children).forEach(starNode => {
         starNode.classList.remove('curr');
      });
      event.target.classList.add('curr');
   }

   return (
      <div className="rating" data-id={id}>
         {
            Object.keys(stars).map(key => stars[key])
         }
      </div>
   )

}
RatingStars.defaultProps = {
   rank: 0
}
RatingStars.propTypes = {
   action: PropTypes.func.isRequired,
   id: PropTypes.number.isRequired,
   rank: PropTypes.number
};
export default RatingStars;
 
