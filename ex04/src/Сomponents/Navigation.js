import React from 'react'
import {NavLink} from 'react-router-dom'

const Navigation = () => {
   return(
      <nav>
         <NavLink activeClassName="active" to="/">Home</NavLink>
         <NavLink activeClassName="active" to="/singers">Singers</NavLink>
         <NavLink activeClassName="active" to="/about">About</NavLink>
         <NavLink activeClassName="active" to="/contacts">Contacts</NavLink>
         <NavLink activeClassName="active" to="/list">List</NavLink>
         <NavLink activeClassName="active" to="/list/item">Item</NavLink>
      </nav>
   )
}

export default Navigation;