import { Home, About, Contacts, Singers, NoFound } from './Pages';
import { Singer, Album } from './Pages/Singers';
import { List, Item } from './Сomponents';

const Routes = [
  {
    path: '/',
    exact: true,
    component: Home
  },
  {
   path: '/singers',
   exact: true, 
   component: Singers
  },
  {
   path: '/singers/:singerId',
   exact: true, 
   component: Singer
  },
  {
   path: '/singers/:singerId/albums/:albumId',
   exact: true, 
   component: Album
  },
  {
   path: '/list',
   exact: true, 
   component: List
  },
  {
   path: '/list/:itemId',
   component: Item
  },
  {
    path: '/contacts',
    component: Contacts
  },
  {
    component: About,
    path: '/about',
  },
  {
    component: NoFound,
    exact: true
  }

];

export default Routes;
