import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Helmet from 'react-helmet';

class Singer extends Component {
   constructor(props) {
      super(props);
      this.state = {
         singer: null,
         singerId: Number(this.props.match.params.singerId)
      }
   }
   countTotalDuration(compositions) { 
      if(compositions.length) {
         let sum = [0, 0];
         const MINUTES = 60;

         compositions.forEach(song => {
            const arrDuration = song.duration.split(':');
            sum[0] += +arrDuration[0];
            sum[1] += +arrDuration[1];
         });

         if( sum[1] >= MINUTES ) {
            sum[0] += ((sum[1] - (sum[1] % MINUTES)) / MINUTES);
            sum[1] = sum[1] % MINUTES;
         }

         return sum[0] + ':' + sum[1];
      } else {
         return null;
      }
   }
   componentDidMount() {
      fetch('http://www.json-generator.com/api/json/get/cuKKbBWWXm?indent=2')
         .then(res => res.json())
         .then(singers => {
            this.setState({
               singer: singers[this.state.singerId]
            })
         })
   }
   render() {
      const {singer, singerId} = this.state;
      const { countTotalDuration } = this;
      if(singer) {
         return(
            <section>
               <Helmet>
                  <title>MyMusicBox:Albums</title>
               </Helmet>
               <h1>{singer.name}</h1>
   
               <ul>
                  {
                     singer.album.map((album, index) => (
                        <li key={index}>
                           <h5>
                              <Link to={'/singers/' + singerId + '/albums/' + index}>
                                    {album.name}
                              </Link>
                           </h5>
                           <p>
                              Сompositions: {album.compositions.length}
                           </p>
                           <p>
                              Album duration: {countTotalDuration(album.compositions)}
                           </p>
                        </li>
                     ))
                  }
               </ul>
            </section>
         )
      } else {
         return(
            <section>
               Loading...
            </section>
         )
      }
   }
}

export default Singer;