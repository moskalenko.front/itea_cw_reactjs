import React, { Component } from 'react';
import { RatingStars } from '../../../Сomponents';
import Helmet from 'react-helmet';

class Album extends Component {
   constructor(props) {
      super(props);
      this.state = {
         album: null,
         singerId: +this.props.match.params.singerId,
         albumId: +this.props.match.params.albumId
      }
   }
   checkRating = (singerId, albumId, compositionId) => {
      const store = this.getStore();

      for(let item of store) {
         if(
            item.singerId === singerId &&
            item.albumId === albumId &&
            item.compositionId === compositionId
         ) {
            return item.rank;
         }
      }
   }
   getStore() {
      const STORE_NAME = 'rating';

      return JSON.parse(localStorage.getItem(STORE_NAME)) || [];
   }
   setStore(data){
      const STORE_NAME = 'rating';
      let store = JSON.parse(localStorage.getItem(STORE_NAME)) || [];
      const findedIndx = store.findIndex(item => {
         return (
            item.singerId === data.singerId &&
            item.albumId === data.albumId &&
            item.compositionId === data.compositionId
         ) ?
         true :
         false
      });
      
      if(findedIndx > -1) {
         store[findedIndx] = data;
      } else {
         store.push(data);
      }
      localStorage.setItem(STORE_NAME, JSON.stringify(store));
   }
   changeRating = (event) => {
      const compositionRating = {
         singerId: this.state.singerId, 
         albumId: this.state.albumId, 
         compositionId: +event.target.parentNode.dataset.id, 
         rank: +event.target.dataset.rank
      }
      this.setStore(compositionRating);
   }
   componentDidMount() {
      fetch('http://www.json-generator.com/api/json/get/cuKKbBWWXm?indent=2')
         .then(res => res.json())
         .then(singers => {
            this.setState({
               album: singers[this.state.singerId].album[this.state.albumId]
            })
         })
   }
   render() {
      const {album, singerId, albumId} = this.state;
      const {changeRating, checkRating} = this;
      
      if(album){
         return(
            <section>
               <Helmet>
                  <title>MyMusicBox:Artist - Album - Song</title>
               </Helmet>
               <h1>{album.name}</h1>
               <hr />
               <h5>Compositions:</h5>
               <ul>
                  {
                     album.compositions.map((song, index) => {
                        return (
                           <li key={index}>
                              {song.name}
                              <RatingStars 
                                 action={changeRating}
                                 rank={checkRating(singerId, albumId, index)}
                                 id={index}
                              />
                           </li>
                        )
                     })
                  }
               </ul>
            </section>
         )
      } else {
         return (<p>Loading...</p>)
      }
   }
}

export default Album;