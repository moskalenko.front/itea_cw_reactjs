import Singers from './Singers'
import Singer from './components/Singer'
import Album from './components/Album'

export {Singers, Singer, Album};