import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Helmet from 'react-helmet';

class Singers extends Component {
   constructor(props) {
      super(props);
      this.state = {
         singers: []
      }
   }
   componentDidMount() {
      fetch('http://www.json-generator.com/api/json/get/cuKKbBWWXm?indent=2')
         .then(res => res.json())
         .then(singers => (
            this.setState({
               singers: singers
            })
         ))
   }
   render() {
      const { singers } = this.state;
      return(
         <>
            <Helmet>
               <title>MyMusicBox:Singers</title>
            </Helmet>
            <ul>
               {
                  singers.map(singer => (
                     <li key={singer.index}>
                        <Link to={'/singers/' + singer.index}>{singer.name}</Link>
                     </li>
                  ))
               }
            </ul>
         </>
      );
   }
}

export default Singers;