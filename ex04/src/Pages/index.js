import Home from './Home';
import About from './About';
import Contacts from './Contacts';
import NoFound from './NoFound';
import Singers from './Singers/Singers';

export {Home, About, Contacts, NoFound, Singers};