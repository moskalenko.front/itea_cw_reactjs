import React, { Component } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Navigation from './Сomponents/Navigation';
import Routes from './routes'
import Helmet from 'react-helmet';

import './App.css';

const supportsHistory = 'pushState' in window.history;

class App extends Component {
   render() {
      return (
         <div id="app">
            <Helmet>
               <title>MyMusicBox</title>
            </Helmet>
            <BrowserRouter forceRefrech={!supportsHistory}>
            <div>
               <Navigation />
               <Switch>
                  {
                     Routes.map( (route, index ) => {
                        return <Route key={index} {...route} />;
                     })
                  }
               </Switch>
            </div>
            </BrowserRouter>
         </div>
      );
   }
}

export default App;
