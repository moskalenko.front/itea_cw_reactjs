export const HALL_FETCHED = 'HALL_FETCHED';

export const BASKET_TOGGLE = 'BASKET_TOGGLE'; 
export const BASKET_REMOVE = 'BASKET_REMOVE'; 
export const BASKET_RESET = 'BASKET_RESET';

export const SOLD_ADD = 'SOLD_ADD'; 
export const SOLD_RESET = 'SOLD_RESET'; 


