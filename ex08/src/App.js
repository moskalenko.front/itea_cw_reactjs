import React from 'react';
import Root from './components/root';
import {Provider} from 'react-redux';
import store from './redux/store'

const App = () =>  (
  <Provider store={store}>
    <Root />
  </Provider>
);

export default App;
