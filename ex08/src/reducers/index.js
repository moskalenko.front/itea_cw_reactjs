import { combineReducers } from 'redux';
import basket from './basket';
import hall from './hall';
import soldSeats from './soldSeats'

const reducer = combineReducers({
    soldSeats,
    basket,
    hall
});

export default reducer;
