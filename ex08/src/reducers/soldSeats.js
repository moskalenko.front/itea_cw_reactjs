import {SOLD_ADD, SOLD_RESET} from './../constants'
import {NAME_SOLD_SEATS} from './../config';

const storeSoldSeats = JSON.parse(localStorage.getItem(NAME_SOLD_SEATS));
const initState = {
    data: storeSoldSeats ? storeSoldSeats : []
};

const soldSeats = (state = initState, action) => {
    switch(action.type) {
        case SOLD_ADD: 
            action.data.forEach(el => {
                state.data.push(el);
            });
            localStorage.setItem(NAME_SOLD_SEATS, JSON.stringify(state.data));
            return {
                ...state,
                data: [...state.data]
            }
        case SOLD_RESET: 
            localStorage.removeItem(NAME_SOLD_SEATS);
            return {
                ...state,
                data: []
            }
        default: 
            return state;
    }   
}

export default soldSeats;