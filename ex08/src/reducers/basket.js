import {BASKET_TOGGLE, BASKET_REMOVE, BASKET_RESET} from './../constants'

const initState = {data:[]};

const basket = (state = initState, action) => {
    switch(action.type) {
        case BASKET_TOGGLE: 
            const findIndexObjSeat = state.data.findIndex((el) => (
                el.nameHall === action.data.nameHall &&
                el.rowIndex === action.data.rowIndex &&
                el.seat.seat === action.data.seat.seat
            ));
            
            if(findIndexObjSeat >= 0) {
                state.data.splice(findIndexObjSeat, 1);
            } else {
                state.data.push(action.data)
            }

            return {
                ...state,
                data: [...state.data]
            }
        case BASKET_REMOVE: 
            const findIndexRemoveSeat = state.data.findIndex((el) => (
                el.nameHall === action.data.nameHall &&
                el.rowIndex === action.data.rowIndex &&
                el.seat.seat === action.data.seat.seat
            ));
            
            if(findIndexRemoveSeat >= 0) {
                state.data.splice(findIndexRemoveSeat, 1);
            }
            
            return {
                ...state,
                data: [...state.data]
            }
        case BASKET_RESET: 
            return {
                ...state,
                data: []
            }
        default: 
            return state;
    }
}

export default basket;