import {HALL_FETCHED} from './../constants'

const initState = null;

const hall = (state = initState, action) => {
    switch(action.type) {
        case HALL_FETCHED: 
            return {
                ...state,
                ...action.data
            }
        default: 
            return state;
    }
}

export default hall;