import React from 'react';
import Hall from './../../hall';
import BasketMenu from './../../basket-menu'
import axios from 'axios';
import {connect} from 'react-redux';
import {URL_BLUE_HALL, URL_RED_HALL, URL_GREEN_HALL} from './../../../config';
import {
    HALL_FETCHED,
    BASKET_TOGGLE, BASKET_REMOVE, BASKET_RESET,
    SOLD_ADD, SOLD_RESET
} from './../../../constants'
import './style.css';


const  MainPage = ({hall, dataBasket, soldSeats, dispatch}) => {
    const fetchRedHall = (e) => {
        axios.get(URL_RED_HALL)
            .then(res => {
                dispatch({
                    type: HALL_FETCHED,
                    data: res.data
                });
                dispatch({
                    type: BASKET_RESET
                })
            });
    }
    const fetchBlueHall = (e) => {
        axios.get(URL_BLUE_HALL)
            .then(res => {
                dispatch({
                    type: HALL_FETCHED,
                    data: res.data
                });
                dispatch({
                    type: BASKET_RESET
                })
            });
    }
    const fetchGreenHall = (e) => {
        axios.get(URL_GREEN_HALL)
            .then(res => {
                dispatch({
                    type: HALL_FETCHED,
                    data: res.data
                });
                dispatch({
                    type: BASKET_RESET
                })
            });
    }
    const toggleToBasket = (nameHall, rowIndex, seat) => {
        const data = {
            nameHall,
            rowIndex,
            seat
        };

        dispatch({
            type: BASKET_TOGGLE,
            data: data
        })
    }
    const removeFromBasket = (nameHall, rowIndex, seat) => {
        const data = {
            nameHall,
            rowIndex,
            seat
        };
        
        dispatch({
            type: BASKET_REMOVE,
            data: data
        });
    }
    const onBuySeats = (data) => {
        dispatch({
            type: SOLD_ADD,
            data: data
        });
        dispatch({
            type: BASKET_RESET
        });
    }
    const resetSoldSeats = () => {
        dispatch({
            type: SOLD_RESET
        });
    }
    return (
        <section className={
            (dataBasket.length) ? 'main-page show-basket' : 'main-page'
        }>
            <div className="main-page_wrap-basket">
                <BasketMenu dataBasket={dataBasket} onBuySeats={onBuySeats} deleteItem={removeFromBasket}/>
            </div>
            <div className="main-page_wrap-hall">
                <h2>Добро пожаловать!</h2>
                <h4>Выберите кинозал:</h4>
                <div className="main-page_hall">
                    <span 
                        className={
                            `main-page_hall main-page_hall--red ${(hall && hall.name === 'red') ? 'active' : null}`
                        } 
                        onClick={fetchRedHall}
                    >
                        Красный кинозал
                    </span>
                    <span 
                        className={
                            `main-page_hall main-page_hall--blue ${(hall && hall.name === 'blue') ? 'active' : null}`
                        } 
                        onClick={fetchBlueHall}
                    >
                        Синий кинозал
                    </span>
                    <span 
                        className={
                            `main-page_hall main-page_hall--green ${(hall && hall.name === 'green') ? 'active': null}`
                        }
                        onClick={fetchGreenHall}
                    >
                        Зеленый кинозал
                    </span>
                </div>
                {
                    hall &&
                    <Hall hall={hall} toggleToBasket={toggleToBasket} soldSeats={soldSeats} dataBasket={dataBasket}/>
                }
                {
                    soldSeats.length > 0 &&
                    <div><button onClick={resetSoldSeats}>Очистить проданные билеты</button></div>
                }
            </div>
        </section>
    )

}
const mapStateToProps = (state, ownProps) => {
    return {
        hall: state.hall,
        dataBasket: state.basket.data,
        soldSeats: state.soldSeats.data
    };
};


export default connect(mapStateToProps)(MainPage);
