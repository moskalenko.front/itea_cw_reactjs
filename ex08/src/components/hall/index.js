import React from 'react';
import PropTypes from 'prop-types';
import './style.css';

const Hall = ({hall, toggleToBasket, dataBasket, soldSeats}) => {
    return(
    
        <div 
            className={`hall hall--${hall.name ? hall.name : 'default'}`}
        >
            <div className="hall_screen">ЕКРАН</div>
            {
                hall.seats.map((row, rowIndex) => (
                    <div key={rowIndex} className="hall_row">
                        <div className="hall_row-title">
                            ряд {rowIndex} 
                        </div>
                        {row.map((seat, seatIndex) => (
                            <div 
                                key={seatIndex} 
                                onClick={(e) => toggleToBasket(hall.name, rowIndex, seat)}
                                className={
                                    soldSeats.some((el) => (
                                        el.nameHall === hall.name &&
                                        el.rowIndex === rowIndex &&
                                        el.seat.seat === seat.seat
                                    )) ? 
                                        'hall_seat buy' : 
                                        dataBasket.some((el) => (
                                            el.nameHall === hall.name &&
                                            el.rowIndex === rowIndex &&
                                            el.seat.seat === seat.seat
                                        )) ?
                                            'hall_seat active':
                                            'hall_seat'
                                }
                                
                            >
                                {seat.seat}
                            </div>
                        ))}
                    </div>
                ))
            }
        </div>
    );
}

Hall.propTypes = {
    hall: PropTypes.object.isRequired,
    toggleToBasket: PropTypes.func.isRequired,
    dataBasket: PropTypes.array.isRequired,
    soldSeats: PropTypes.array.isRequired
}
export default Hall;
