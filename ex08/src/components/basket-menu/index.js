import React from 'react';
import PropTypes from 'prop-types';
import './style.css'

const BasketMenu = ({dataBasket, deleteItem, onBuySeats}) => (
    dataBasket.length && (
    <div className={ `basket-menu basket-menu--${dataBasket[0].nameHall}`}>
        {
            dataBasket.map((el, index) => (
                <div key={index} className="basket-menu_item">
                    <div className="basket-menu_item-inner">
                        <span className="basket-menu_item-title">
                            Ряд:
                        </span>
                        {el.rowIndex}
                    </div>
                    <div className="basket-menu_item-inner">
                        <span className="basket-menu_item-title">
                            Место:
                        </span>
                        {el.seat.seat}
                    </div>
                    <div className="basket-menu_item-inner">
                        <span className="basket-menu_item-title">
                            Цена:
                        </span>
                        {el.seat.price}грн
                    </div>
                    <div onClick={(e) => deleteItem(el.nameHall, el.rowIndex, el.seat)} className="basket-menu_item-inner i-del">
                        &#10006;
                    </div>
                </div>
            ))
        }
        <button type="button" className="btn-buy" onClick={(e) => onBuySeats(dataBasket)}>
            Купить билеты 
            {' ' + dataBasket.reduce((accumulator, curr) => accumulator + curr.seat.price, 0) + ' ' }
            грн
        </button>
    </div>)
)


BasketMenu.propTypes = {
    deleteItem: PropTypes.func.isRequired,
    onBuySeats: PropTypes.func.isRequired,
    dataBasket: PropTypes.array.isRequired
}
export default BasketMenu;