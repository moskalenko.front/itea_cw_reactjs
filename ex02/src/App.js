import React, { Component } from 'react';
import './App.css';
import Button from './components/Button'
import User from './components/User'

class App extends Component {
   constructor(props) {
      super(props);
      this.state = {
         users: []
      }
   }
   changeStatus = (user) => {
      let users = this.state.users.map(usr => {
         if (usr === user) {
            usr.interviewed = !usr.interviewed;
         }
         return usr;
      });
      this.setState({
         users: users
      })
   }
   componentDidMount() {
      fetch('http://www.json-generator.com/api/json/get/cftYlbPMZe?indent=2')
         .then(res => res.json())
         .then(users => {
            users = users.map(user => ({
               interviewed: false,
               user: user
            }))

             this.setState({users: users})
         });
   }
   render() {
      const {users} = this.state;
      return (
         <div>
            <ul>
               {
                  users.map((user, index) => (
                     <User 
                        key={index} 
                        user={user} 
                        action={() => this.changeStatus(user)}
                     />
                  ))
               }
            </ul>
            <Button 
               style= {
                  {
                     padding: '10px 20px',
                     border: 'none',
                     backgroundColor: '#000',
                     color: '#fff'
                  } 
               }
               action = {
                  (event) => {alert(1)}
               }
            />
         </div>
      );
   }
}

export default App;
