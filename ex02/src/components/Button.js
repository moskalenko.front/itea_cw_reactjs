import React from 'react'

const Button = ({style, action}) => {
   return (
      <button 
         style={style}
         onClick={action}
      >
         btn
      </button>
   )
}

export default Button;