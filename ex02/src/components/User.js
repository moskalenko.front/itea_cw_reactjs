import React from 'react'

const User = ({user, action}) => {
   return(
      <li 
         className={user.interviewed ? 'interviewed' : ''}
         onClick={action}
      >
         {JSON.stringify(user)}
      </li>
   );
}

export default User;